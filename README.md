# TemplateFileHelper module #

**TemplateFileHelper** is a Processwire CMS / CMF module and provides some TemplateFile class helpers to structure the PW layout / templates.

## Documentation and support ##

* [Documentation](https://bitbucket.org/pwFoo/templatefilehelper/wiki/Documentation)
* [Issues](https://bitbucket.org/pwFoo/templatefilehelper/issues?status=new&status=open) and [Processwire support forum](https://processwire.com/talk/topic/14335-templatefilehelper/)
* [Source code](https://bitbucket.org/pwFoo/templatefilehelper/src/master)
* [Download current release](https://bitbucket.org/pwFoo/templatefilehelper/get/master.zip)